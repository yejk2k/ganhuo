;
(function() {
    AV.initialize("tcp0zp2t0lfvh8vf2g4x849hotv1rv1r5k2bilbrntsn3b90", "c96hgictontpudtxs2pwwr0hq7ykbvuymvnoyrh3uax4ggl2");
    var gh_modal;

    function gh_close_modal() {
        gh_modal.close();
        $("#gh_modal_wrapper").remove();
    }

    function submit_ganhuo() {
        var content = $("#gh_description").val();
        var url = window.location.url;
        var type = $("#gh_type").val();
        var who = $("#gh_who").val();

        if (!content || content.length == 0) {
            alert('请填写描述语');
            return;
        }

        if (!who || who.length == 0) {
            alert('请填写你的 ID');
            return;
        }

        var Ganhuo = AV.Object.extend("ganhuo");
        var ganhuo = new Ganhuo();

        var query = new AV.Query(Ganhuo);
        query.equalTo("url", window.location.href);
        query.find({
            success: function(results) {
                if (results.length > 0) {
                    toastr.error("干货战斗小组", "不好！ 重复了...")
                } else {
                    ganhuo.save({
                        url: window.location.href,
                        desc: content,
                        who: who,
                        type: type
                    }, {
                        success: function(object) {
                            gh_modal.close();
                            toastr.success("干货战斗小组", "成功提交啦！");
                        },
                        error: function() {
                            toastr.error("干货战斗小组", "联网出问题了");
                        }
                    });
                }
            },
            error: function() {
                toastr.error("干货战斗小组", "联网出问题了");
            }
        })
    }

    if(!document.getElementById("gh_modal_wrapper")) {
       $.get(chrome.extension.getURL('modal.html'), function(data) {
        $("body").append(data);
        gh_modal = new RModal(document.getElementById("gh_modal_wrapper"), {
            beforeOpen: function(next) {
                next();
            },
            afterOpen: function() {
                $('#gh_close').click(gh_close_modal);
                $('#gh_submit').click(submit_ganhuo);
                var gh_link_selector = $("#gh_link");
                gh_link_selector.attr('href', window.location.href);
                gh_link_selector.html(window.location.href);
            },
            beforeClose: function(next) {
                next();
            },
            afterClose: function() {

            },
            dialogClass: "gh_modal",
            bodyClass: "gh_modal_wrapper"
        });
        gh_modal.open();
    }); 
    }
    
})();